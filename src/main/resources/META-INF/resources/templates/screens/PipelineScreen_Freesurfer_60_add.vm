##Copyright 2009 Washington University in St. Louis All Rights Reserved

##<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">

#set ($template = $data.getTemplateInfo())
$!template.setLayoutTemplate("/Popup_empty.vm")

#set ($project = $om.getProject())
#set ($schemaType = $newpipeline.getSchemaElementName())
#set ($parameters =$newpipeline.getParameters_parameter())

##<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script language="Javascript">
    window.resizeTo(650,450);

    $(document).prop("title","Configure FreeSurfer Pipeline for $project");

    $(document).ready(function(){
        $("#button-submit").on("click",function(){
            // Validate that T1 scan types param is not blank
            if (! $("#params-mr_scan_types").val().trim() ) {
                alert("You must enter the T1 scan type(s) for your project.");
                $("#params-mr_scan_types").css("border","1px solid red")
                .animate({
                    borderWidth: "1"
                },200,function(){
                    $(this).css("border","1px solid #999");
                });
                return false;
            }


            for (var config in configData) {
                configData[config].saveConfig();
            }
            $("#submitForm").click();
        });
    });

    // toggle visibility of advanced parameters. (parameter values are always passed to form, whether or not they are visible)
    function advanced_params_toggle(){
        var adv = $("#advanced-params");
        if ($(adv).hasClass("hidden")) {
            $(this).html("Hide");
            $(adv).removeClass("hidden");
        } else {
            $(this).html("Show");
            $(adv).addClass("hidden");
        }
    }

</script>
<style>
    html body.popup {
        background: rgb(240, 240, 240) !important;
    }
</style>

<div id="pipeline_modal" class="xmodal fixed embedded open" style="max-width: 100%; max-height: 100%; margin: 0;">
    <div class="body content scroll">
        <!-- handle error messages -->
        #if ($data.message)
        <div class="error">$data.message</div>
        #end

        <div class="inner">
        <!-- modal body start -->

            <form id="freesurferoptions" class="friendlyForm" name="freesurferoptions" method="post" action="$link.setAction("ManagePipeline")">

                <h2>Set up Freesurfer 6.0 for $project </h2>

                <p><strong> Please define the following default parameters for your project. </strong></p>

                #set($i=0)
                #foreach ($parameter in $parameters)
                    <input type="hidden" id="${schemaType}.parameters.parameter[$i].name" name="${schemaType}.parameters.parameter[$i].name" value="$!parameter.getName()"/>
                    <input type="hidden" id="${schemaType}.parameters.parameter[$i].description" name="${schemaType}.parameters.parameter[$i].description" value="$!parameter.getDescription()"/>
                    #if ($i==3)
                    <p>
                        <label for="${schemaType}.parameters.parameter[$i].csvValues" title="recon-all args">Arguments to pass to recon-all</label>
                        <input type="text" id="${schemaType}.parameters.parameter[$i].csvValues" name="${schemaType}.parameters.parameter[$i].csvValues" value="$!parameter.getCsvvalues()" size="75"/>
                    </p>
                    #else
                        #if ($!parameter.getSchemalink())
                        <input type="hidden" id="${schemaType}.parameters.parameter[$i].schemaLink" name="${schemaType}.parameters.parameter[$i].schemaLink" value="$!parameter.getSchemalink()"/>
                        #elseif ($!parameter.getCsvvalues())
                        <input type="hidden" id="${schemaType}.parameters.parameter[$i].csvValues" name="${schemaType}.parameters.parameter[$i].csvValues" value="$!parameter.getCsvvalues()"/>
                        #end
                    #end
                    #set ($i = $i+1)
                #end

                #set($scanTypeToolName="params")
                #set($scanTypeFileName="mr_scan_types")
                #set($nullable = "true")
                #config($scanTypeToolName $scanTypeFileName $project $nullable)
                <script>
                    var labelNode = $("#${scanTypeToolName}-${scanTypeFileName}-label");
                    labelNode.attr("title","Scan types");
                    labelNode.text("What are the scan types for T1 scans in your project?");
                </script>

                #set($scanTypeToolName="params")
                #set($scanTypeFileName="t2_scan_types")
                #set($nullable = "true")
                #config($scanTypeToolName $scanTypeFileName $project $nullable)
                <script>
                    var labelNode = $("#${scanTypeToolName}-${scanTypeFileName}-label");
                    labelNode.attr("title","Scan types");
                    labelNode.text("If you use them, what are the scan types for T2 scans in your project?");
                </script>

                #set($scanTypeToolName="params")
                #set($scanTypeFileName="flair_scan_types")
                #set($nullable = "true")
                #config($scanTypeToolName $scanTypeFileName $project $nullable)
                <script>
                    var labelNode = $("#${scanTypeToolName}-${scanTypeFileName}-label");
                    labelNode.attr("title","Scan types");
                    labelNode.text("If you use them, what are the scan types for FLAIR scans in your project?");
                </script>


                <div class="hidden">
                    <input type="submit" id="submitForm" name="eventSubmit_doAddprojectpipeline" value="Submit"/>
                </div>

                <input type="hidden" id="${schemaType}.stepId" name="${schemaType}.stepId" value="Freesurfer_6.0"/>
                <input type="hidden" id="${schemaType}.displayText" name="${schemaType}.displayText" value="Freesurfer 6.0"/>
                <input type="hidden" id="${schemaType}.name" name="${schemaType}.name" value="Freesurfer_6.0"/>
                <input type="hidden" id="${schemaType}.location" name="${schemaType}.location" value="$newpipeline.getLocation()"/>
                <input type="hidden" id="${schemaType}.customwebpage" name="${schemaType}.customwebpage" value="$newpipeline.getCustomwebpage()"/>
                <input type="hidden" id="${schemaType}.description" name="${schemaType}.description" value='$newpipeline.getDescription()'/>

                <input type="hidden" name="search_element" value="xnat:projectData"/>
                <input type="hidden" name="search_field" value="xnat:projectData.ID"/>
                <input type="hidden" name="search_value" value="$project"/>
                <input type="hidden" name="project" value="$project"/>
                <input type="hidden" name="pipeline_path" value="$pipeline_path"/>
                <input type="hidden" name="dataType" value="$dataType"/>
                <input type="hidden" name="schemaType" value="$schemaType"/>
                <input type="hidden" name="edit" value="$edit"/>
                <input type="hidden" name="destination" value="JS_Parent_Return.vm"/>


            </form>
        <!-- modal body end -->
        </div>
    </div>

    <div class="footer">
        <!-- multi-panel controls are placed in footer -->
        <div class="inner">
            <span class="buttons">
                <a id="button-submit" class="default button panel-toggle" href="javascript:" >Configure Pipeline</a>
                <a class="cancel close button" href="javascript:self.close()">Cancel</a>
            </span>
        </div>
    </div>
</div>
<!-- end of xmodal container -->
