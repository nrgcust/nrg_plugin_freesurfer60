package org.nrg.xnat.generated.plugin;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "nrg_plugin_freesurfer60", name = "XNAT 1.7 FreeSurfer 60 Plugin", description = "This is the XNAT 1.7 FreeSurfer 60 Plugin.")
@ComponentScan({"org.nrg.xnat.workflow.listeners"})
public class Freesurfer60Plugin {
}